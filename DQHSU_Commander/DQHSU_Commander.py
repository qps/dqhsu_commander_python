"""self.register_map.HSU_trigger_status
Classes to interface with a DQHSU crate using SPI
@author: gmarting
27th June 2020
"""

from ni_8451 import *
import time
from aenum import Enum, skip

class DQHSU_register_map():
    def __init__(self, register_map=0):
        if register_map == 0:
            self.register_map = bytes(48)
        elif isinstance(register_map, bytes):
            self.register_map = register_map

    @property
    def register_map(self):
        return self.__register_map

    @register_map.setter
    def register_map(self, register_map):
        if isinstance(register_map, bytes):
            self.__register_map = register_map
        elif isinstance(register_map, str):
            self.__register_map = bytes.fromhex(register_map)

    @register_map.getter
    def register_map(self):
        return self.__register_map

    @property
    def dummy(self):
        return self.__register_map[0]
    
    @property
    def rev(self):
        return self.__register_map[1]

    @property
    def HDS_U1(self):
        return int.from_bytes(self.__register_map[2:4], byteorder = "big", signed = False)

    @property
    def HDS_U2(self):
        return int.from_bytes(self.__register_map[4:6], byteorder = "big", signed = False)

    @property
    def HDS_U3(self):
        return int.from_bytes(self.__register_map[6:8], byteorder = "big", signed = False)

    @property
    def HDS_U4(self):
        return int.from_bytes(self.__register_map[8:10], byteorder = "big", signed = False)
    
    @property
    def HDS_I1(self):
        return int.from_bytes(self.__register_map[10:12], byteorder = "big", signed = False)

    @property
    def HDS_I2(self):
        return int.from_bytes(self.__register_map[12:14], byteorder = "big", signed = False)

    @property
    def HDS_I3(self):
        return int.from_bytes(self.__register_map[14:16], byteorder = "big", signed = False)

    @property
    def HDS_I4(self):
        return int.from_bytes(self.__register_map[16:18], byteorder = "big", signed = False)

    @property
    def HDS_T1(self):
        return int.from_bytes(self.__register_map[18:20], byteorder = "big", signed = False)

    @property
    def HDS_T2(self):
        return int.from_bytes(self.__register_map[20:22], byteorder = "big", signed = False)

    @property
    def HDS_T3(self):
        return int.from_bytes(self.__register_map[22:24], byteorder = "big", signed = False)

    @property
    def HDS_T4(self):
        return int.from_bytes(self.__register_map[24:26], byteorder = "big", signed = False)

    @property
    def HSU_status(self):
        return self.__register_map[26]

    @property
    def IFS_conn(self):
        return self.__register_map[27]

    @property
    def HSU_trigger_status(self):
        return int.from_bytes(self.__register_map[28:30], byteorder = "big", signed = False)

    @property
    def rel_time(self):
        return int.from_bytes(self.__register_map[30:34], byteorder = "big", signed = False)

    @property
    def time_flag(self):
        return self.__register_map[34]
    
    @property
    def testmode_relays_status(self):
        return int.from_bytes(self.__register_map[35:37], byteorder = "big", signed = False)

    @property
    def sys_config(self):
        return self.__register_map[37]
    
    @property
    def testmode_relays(self):
        return int.from_bytes(self.__register_map[38:40], byteorder = "big", signed = False)
    
    @property
    def HSU_ADC_speed(self):
        return self.__register_map[40]

    @property
    def HSU_decimation_factor_mux(self):
        return self.__register_map[41]
    
    @property
    def HSU_buf_speed(self):
        return self.__register_map[42]
    
    @property
    def HSU_buffer_trigger_mux(self):
        return self.__register_map[43]
    
    @property
    def HDS_trigger_link_threshold(self):
        return self.__register_map[44]
    
    @property
    def HDS_trigger_link_ADC_speed(self):
        return self.__register_map[45]
    
    @property
    def HDS_trigger_link_decimation_factor_filter_depth(self):
        return self.__register_map[46]
    
    @property
    def HDS_trigger_link_signal_mux(self):
        return self.__register_map[47]
    
    @property
    def startup_option(self):
        return self.__register_map[59]

    @property
    def board_ID(self):
        return int.from_bytes(self.__register_map[60:63], byteorder = "big", signed = False)

class DQHSU_commands(Enum):
    dummy = 0x00
    std_read = 0x01
    NOP = 0x03
    HDS_fuse_relay_on = 0x04
    HSU_fuse_relay_off = 0x05
    HSU_I_relay_on = 0x06
    HSU_T_relay_on = 0x07
    buffer_read = 0x0C
    buffer_rearm = 0x0D
    buffer_trigger = 0x13
    save_to_flash = 0x20
    save_startup_to_flash = 0x21
    save_sn_to_flash = 0x23
    load_from_flash = 0x24
    trigger_startup = 0x25
    update_mem = 0x33
    stop_read = 0x33
    reset = 0x3F

    @skip
    class read(Enum):
        test_reg = 0x40
        sv_rev = 0x41
        HDS_U1_h = 0x42
        HDS_U1_l = 0x43
        HDS_U2_h = 0x44
        HDS_U2_l = 0x45
        HDS_U3_h = 0x46
        HDS_U3_l = 0x47
        HDS_U4_h = 0x48
        HDS_U4_l = 0x49

        HDS_I1_h = 0x4A
        HDS_I1_l = 0x4B
        HDS_I2_h = 0x4C
        HDS_I2_l = 0x4D
        HDS_I3_h = 0x4E
        HDS_I3_l = 0x4F
        HDS_I4_h = 0x50
        HDS_I4_l = 0x51

        HDS_T1_h = 0x52
        HDS_T1_l = 0x53
        HDS_T2_h = 0x54
        HDS_T2_l = 0x55
        HDS_T3_h = 0x56
        HDS_T3_l = 0x67
        HDS_T4_h = 0x58
        HDS_T4_l = 0x59

        IFS_conn = 0x5A
        buffer_status = 0x5B

        trigger_status_h = 0x5C
        trigger_status_l = 0x5D

        rel_time_h = 0x5E
        rel_time_mh = 0x5F
        rel_time_ml = 0x60
        rel_time_l = 0x61

        time_flag = 0x62

        testmode_relays_status_h = 0x63
        testmode_relays_status_l = 0x64

        sys_cfg = 0x65

        testmode_relays_h = 0x66
        testmode_relays_l = 0x67

        HSU_ADC_speed = 0x73
        HSU_decimation_factor_mux = 0x69
        HSU_buf_speed = 0x6A
        HSU_buffer_trigger_mux = 0x6B
        HSU_trig_threshold = 0x6C
        HSU_trig_ADC_speed = 0x6D
        HSU_trigger_link_settings = 0x6E

        HSU_trig_signal_mux = 0x6F
        startup_option = 0x7B

    @skip
    class write(Enum):
        sys_cfg = 0xA5
        testmode_relays_h = 0xA6
        testmode_relays_l = 0xA7

        HSU_decimation_factor_mux = 0xA9
        HSU_buf_speed = 0xAA
        HSU_buffer_trigger_mux = 0xAB
        HSU_trig_threshold = 0xAC
        HSU_trig_ADC_speed = 0xAD
        HSU_trigger_link_settings = 0xAE
        HSU_trig_signal_mux = 0xAF

        HSU_ADC_speed = 0xB3

        startup_option = 0xBB

    @skip
    class confirm_write(Enum):
        sys_cfg = 0xE5
        testmode_relays_h = 0xE6
        testmode_relays_l = 0xE7

        HSU_decimation_factor_mux = 0xE9
        HSU_buf_speed = 0xEA
        HSU_buffer_trigger_mux = 0xEB
        HSU_trig_threshold = 0xEC
        HSU_trig_ADC_speed = 0xED
        HSU_trigger_link_settings = 0xEE
        HSU_trig_signal_mux = 0xEF

        HSU_ADC_speed = 0xF3

        startup_option = 0xFB

class DQHSU(object):
    def __init__(self, spi_object, cs):
        """
        Class initialization. 
        Parameters
        ----------
            spi_object : spi communication object. Requires this methods:
                -spi_config(): configures the device for spi communication
                -spi_writeread(byte list): writes bytes into spi and returns the reads
                -spi_set_CS(cs): (sets chip select on the spi line
            address : Address (default 0).
            clock_rate : Clock rate in kilohertz (default 100).
        Returns
        -------
            None
        """
        self.spi_object = spi_object
        self.cs = cs

        self.spi_object.spi_config(clock_rate = 1000, clock_polarity = 0, clock_phase = 1)

        self.raw_register_map = []

        self.register_map = DQHSU_register_map()

        self.raw_buffer = []

        self.buffer = {}

    def restart(self):
        self.spi_object.spi_set_CS(self.cs)
        self.spi_object.spi_writeread([DQHSU_commands.reset.value])

    def trigger(self):
        self.spi_object.spi_set_CS(self.cs)
        self.spi_object.spi_writeread([DQHSU_commands.buffer_trigger.value])

    def update_register_map(self):
        self.spi_object.spi_set_CS(self.cs)
        self.spi_object.spi_writeread([0x40] + [DQHSU_commands.NOP.value])
        self.raw_register_map = self.spi_object.spi_writeread([DQHSU_commands.dummy.value] * 60 + [DQHSU_commands.stop_read.value])
        self.register_map.register_map = self.raw_register_map
    
    def read_block(self, command=DQHSU_commands.buffer_read.value):
        self.spi_object.spi_set_CS(self.cs)
        self.spi_object.spi_writeread([command] + [DQHSU_commands.NOP.value])
        blkread = self.spi_object.spi_writeread([DQHSU_commands.dummy.value] * 31 + [DQHSU_commands.stop_read.value])
        return blkread

    def read_n_bytes(self, nbytes, blocks, command=DQHSU_commands.buffer_read.value):
        cut_beginning = 3
        cut_ending = 1

        self.spi_object.spi_set_CS(self.cs)
        command_array = [command , DQHSU_commands.NOP.value]
        command_array += [DQHSU_commands.dummy.value] * nbytes
        command_array += [DQHSU_commands.stop_read.value]

        data_read  = []
        for i in range(0, blocks):
            spird = self.spi_object.spi_writeread(command_array)
            data_read.append(spird[cut_beginning:-cut_ending])

        return data_read

    def write_register(self, register, value):
        self.spi_object.spi_set_CS(self.cs)
        register_confirm = register | 0x40
        self.spi_object.spi_writeread([register, value, register_confirm])
        
    @property
    def buffer_status(self):
        spird = self.spi_object.spi_writeread([DQHSU_commands.read.buffer_status.value, 0x00, 0x00, DQHSU_commands.stop_read.value])
        value = spird[1]
        status = {}
        status["recording"] = True if value & 0x01 else False
        status["readout_ready"] = True if value & 0x02 else False
        status["readout_ongoing"] = True if value & 0x04 else False
        status["dready"] = True if value & 0x08 else False
        status["readout_finished"] = True if value & 0x10 else False
        status["buffer_selftrigger"] = True if value & 0x20 else False
        return status

    @property
    def trigger_status(self):
        spird = self.spi_object.spi_writeread([DQHSU_commands.read.trigger_status_h.value, 0x00, 0x00, DQHSU_commands.stop_read.value])
        value = int.from_bytes(spird[1:3], byteorder = "big", signed = False)
        status = {}
        status["HSU2_discharge"] = True if value & 0x01 else False
        status["HSU3_discharge"] = True if value & 0x02 else False
        status["HSU4_discharge"] = True if value & 0x04 else False
        status["discharge_to_HSU2"] = True if value & 0x08 else False
        status["discharge_to_HSU3"] = True if value & 0x10 else False
        status["discharge_to_HSU4"] = True if value & 0x20 else False
        status["HDS1_trigger_link"] = True if value & 0x40 else False
        status["HDS2_trigger_link"] = True if value & 0x80 else False
        status["HDS3_trigger_link"] = True if value & 0x100 else False
        status["HDS4_trigger_link"] = True if value & 0x200 else False
        status["HDS1_trigger_link_OK"] = True if value & 0x400 else False
        status["HDS2_trigger_link_OK"] = True if value & 0x800 else False
        status["HDS3_trigger_link_OK"] = True if value & 0x1000 else False
        status["HDS4_trigger_link_OK"] = True if value & 0x2000 else False
        return status

    @property
    def testmodes_status(self):
        spird = self.spi_object.spi_writeread([DQHSU_commands.read.testmode_relays_status_h.value, 0x00, 0x00, DQHSU_commands.stop_read.value])
        value = int.from_bytes(spird[1:3], byteorder = "big", signed = False)
        status = {}
        status["HDS1_testmode_I_enabled"] = True if value & 0x01 else False
        status["HDS1_testmode_fuse_enabled"] = True if value & 0x02 else False
        status["HDS1_testmode_T_enabled"] = True if value & 0x04 else False
        status["HDS2_testmode_I_enabled"] = True if value & 0x08 else False
        status["HDS2_testmode_fuse_enabled"] = True if value & 0x10 else False
        status["HDS2_testmode_T_enabled"] = True if value & 0x20 else False
        status["HDS3_testmode_I_enabled"] = True if value & 0x40 else False
        status["HDS3_testmode_fuse_enabled"] = True if value & 0x80 else False
        status["HDS3_testmode_T_enabled"] = True if value & 0x100 else False
        status["HDS4_testmode_I_enabled"] = True if value & 0x200 else False
        status["HDS4_testmode_fuse_enabled"] = True if value & 0x400 else False
        status["HDS4_testmode_T_enabled"] = True if value & 0x800 else False
        return status

    @testmodes_status.setter
    def testmodes_status(self, value):
        if isinstance(value, int):
            val_h = value & 0x00FF
            val_l = value & 0xFF00 >> 8

            self.write_register(DQHSU_commads.write.testmode_relays_h, val_h)
            self.write_register(DQHSU_commads.write.testmode_relays_l, val_l)

        elif isinstance(value, dict):
            writeval = 0
            writeval += status["HDS1_testmode_I_enabled"] * 0x01
            writeval += status["HDS1_testmode_fuse_enabled"] * 0x02
            writeval += status["HDS1_testmode_T_enabled"] * 0x04
            writeval += status["HDS1_testmode_I_enabled"] * 0x08
            writeval += status["HDS1_testmode_fuse_enabled"] * 0x10
            writeval += status["HDS1_testmode_T_enabled"] * 0x20
            writeval += status["HDS1_testmode_I_enabled"] * 0x40
            writeval += status["HDS1_testmode_fuse_enabled"] * 0x80
            writeval += status["HDS1_testmode_T_enabled"] * 0x100
            writeval += status["HDS1_testmode_I_enabled"] * 0x200
            writeval += status["HDS1_testmode_fuse_enabled"] * 0x400
            writeval += status["HDS1_testmode_T_enabled"] * 0x800

    @property
    def decimation_factor_mux(self):
        spird = self.spi_object.spi_writeread([DQHSU_commands.read.HSU_decimation_factor_mux.value, 0x00, DQHSU_commands.stop_read.value])
        value = spird[1]
        decimation = {}
        decimation["factor"] = value & 0x07
        decimation["source"] = "fixed" if value & 0x08 else "auto"
        return decimation

    @decimation_factor_mux.setter
    def decimation_factor_mux(self, decimation):
        if isinstance(decimation, dict):
            value = 0
            value = decimation["factor"] & 0x07
            value = value | 0x08 if decimation["source"] == "fixed" else value
            self.write_register(DQHSU_commands.write.HSU_decimation_factor_mux.value, value)

        elif isinstance(decimation, int):
            self.write_register(DQHSU_commands.write.HSU_decimation_factor_mux.value, decimation)

    @property
    def buffer_trigger_mux(self):
        spird = self.spi_object.spi_writeread([DQHSU_commands.read.HSU_buffer_trigger_mux.value, 0x00, DQHSU_commands.stop_read.value])
        value = spird[1]
        tmp_comp = value & 0x03
        trig_src = {}
        if tmp_comp == 0:
            trig_src["command"] = False
            trig_src["quench_trigger_line"] = True
        elif tmp_comp == 1:
            trig_src["command"] = True
            trig_src["quench_trigger_line"] = False
        elif tmp_comp == 2:
            trig_src["command"] = True
            trig_src["quench_trigger_line"] = True
        elif tmp_comp == 3:
            trig_src["command"] = False
            trig_src["quench_trigger_line"] = True
        
        return trig_src

    @buffer_trigger_mux.setter
    def buffer_trigger_mux(self, value):
        if isinstance(value, dict):
            if value["command"]:
                if value["quench_trigger_line"]:
                    writeval = 2
                else:
                    writeval = 1
            else:
                writeval = 0

            self.write_register(DQHSU_commands.write.HSU_buffer_trigger_mux.value, decimation)

        elif isinstance(value, int):
            self.write_register(DQHSU_commands.write.HSU_buffer_trigger_mux.value, decimation)

    @property
    def trigger_link_settings(self):
        spird = self.spi_object.spi_writeread([DQHSU_commands.read.HDS_trigger_link_decimation_factor_filter_depth.value, 0x00, DQHSU_commands.stop_read.value])
        value = spird[1]
        settings = {}
        settings["decimation_factor"] = value & 0x0F
        settings["filter_depth"] = (value & 0xF0) >> 4
        return settings

    @trigger_link_settings.setter
    def trigger_link_settings(self, value):
        if isinstance(value, int):
            self.write_register(DQHSU_commands.write.HSU_trigger_link_settings.value, value)

        elif isinstance(value, dict):
            writeval = 0
            writeval = settings["decimation_factor"] & 0x0F
            writeval + settings["filter_depth"] << 4
            self.write_register(DQHSU_commands.write.HSU_trigger_link_settings.value, writeval)

    def read_buffer(self):
        samples_per_block = 1024
        max_samples = 1024 * 16
        blksize = 29
        buffer = []
        for i in range(0, int(max_samples / samples_per_block)):
            buffer += self.read_n_bytes(blksize, samples_per_block)
            print("read " + str((i+1)*samples_per_block) + " bytes")
        self.raw_buffer = buffer
        
        self.buffer["HDS_U1"] = [int.from_bytes(item[0:2], byteorder = "big", signed = False) for item in self.raw_buffer]
        self.buffer["HDS_U2"] = [int.from_bytes(item[2:4], byteorder = "big", signed = False) for item in self.raw_buffer]
        self.buffer["HDS_U3"] = [int.from_bytes(item[4:6], byteorder = "big", signed = False) for item in self.raw_buffer]
        self.buffer["HDS_U4"] = [int.from_bytes(item[6:8], byteorder = "big", signed = False) for item in self.raw_buffer]

        self.buffer["HDS_I1"] = [int.from_bytes(item[8:10], byteorder = "big", signed = False) for item in self.raw_buffer]
        self.buffer["HDS_I2"] = [int.from_bytes(item[10:12], byteorder = "big", signed = False) for item in self.raw_buffer]
        self.buffer["HDS_I3"] = [int.from_bytes(item[12:14], byteorder = "big", signed = False) for item in self.raw_buffer]
        self.buffer["HDS_I4"] = [int.from_bytes(item[14:16], byteorder = "big", signed = False) for item in self.raw_buffer]
        
        self.buffer["HDS_T1"] = [int.from_bytes(item[16:18], byteorder = "big", signed = False) for item in self.raw_buffer]
        self.buffer["HDS_T2"] = [int.from_bytes(item[18:20], byteorder = "big", signed = False) for item in self.raw_buffer]
        self.buffer["HDS_T3"] = [int.from_bytes(item[20:22], byteorder = "big", signed = False) for item in self.raw_buffer]
        self.buffer["HDS_T4"] = [int.from_bytes(item[22:24], byteorder = "big", signed = False) for item in self.raw_buffer]

if __name__ == "__main__":
    spi_device = NI845x()
    hsu1 = DQHSU(spi_device, 1)
    hsu2 = DQHSU(spi_device, 2)
    hsu1.restart()
    time.sleep(2)
    hsu1.trigger()
    time.sleep(1)
    print(hsu1.buffer_status)
    hsu1.read_buffer()
    pass
    #while True:
    #    i += 1
    #    time.sleep(0.5)
    #    hsu1.update_register_map()
    #    hsu2.update_register_map()

    #    print(hsu1.register_map.register_map)
    #    print(hsu2.register_map.register_map)
    #    print(hsu1.decimation_factor_mux)
    #    hsu1.decimation_control = {"factor" : i, "source" : "fixed"}
    #    print("")
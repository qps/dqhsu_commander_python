"""
Classes to control and interface with a NI 8451 device through python
Based on the work from https://github.com/bensondaled/ni845x
@author: gmarting
19th May 2019
"""

import ctypes
import numpy as np
import time

MAX_SIZE = 1024
DEV_SIZE = 256

ni845x_dll = ctypes.windll.LoadLibrary('Ni845x.dll')

class Ni845xError(Exception):
    def __init__(self, status_code):
        message = ctypes.create_string_buffer(MAX_SIZE)
        ni845x_dll.ni845xStatusToString(status_code, MAX_SIZE, message)
        Exception.__init__(self, message.value)
        
def errChk(err):
    if err:
        raise Ni845xError(err)
    pass

class NI845x():
    VOLTS33 = 33
    VOLTS25 = 25
    VOLTS18 = 18
    VOLTS15 = 15
    VOLTS12 = 12
    INPUT,OUTPUT = 0,1

    kNi845xI2cNakFalse = 0
    kNi845xI2cNakTrue = 1

    kNi845xHSDisable = 0
    kNi845xHSEnable = 1

    kNi845xPullupDisable = 0
    kNi845xPullupEnable = 1

    kNi845xI2cAddress7Bit = 0
    kNi845xI2cAddress10Bit = 1

    kNi845xSpiClockPolarityIdleLow = 0
    kNi845xSpiClockPolarityIdleHigh = 1

    kNi845xSpiClockPhaseFirstEdge = 0
    kNi845xSpiClockPhaseSecondEdge = 1
    """Static method. Outputs the number of available devices
    """
    def available_devices():
        FirstDevice = ctypes.create_string_buffer(DEV_SIZE)
        FindDeviceHandle = ctypes.c_uint32()
        NumberFound = ctypes.c_uint32()

        err = ni845x_dll.ni845xFindDevice(ctypes.byref(FirstDevice), ctypes.byref(FindDeviceHandle), ctypes.byref(NumberFound))
        errChk(err)

        return NumberFound

    def __init__(self):
        # Determine available devices
        self.__FirstDevice = ctypes.create_string_buffer(DEV_SIZE)
        self.__FindDeviceHandle = ctypes.c_uint32()
        self.__NumberFound = ctypes.c_uint32()
                
        err = ni845x_dll.ni845xFindDevice(ctypes.byref(self.__FirstDevice), ctypes.byref(self.__FindDeviceHandle), ctypes.byref(self.__NumberFound))
        errChk(err)

        if self.__NumberFound.value != 1:
            raise Exception('Only implemented support for exactly 1 USB card. {} found.'.format(self.__NumberFound.value))

        self.__name = self.__FirstDevice
        
        self.__open()
        self.i2c_config()
        self.dio_set_voltage_level(self.VOLTS33)
        self.dio_set_port_line_direction_map(self.OUTPUT * np.ones(8))
        self.dio_set_push_pull()

    def i2c_config(self, size=0, address=0, clock_rate=100, timeout=2000):
        """
        Set the ni845x I2C configuration.
        
        Parameters
        ----------
            size : Address size  0=7Bit, 1=10Bit.
            address : Address (default 0).
            clock_rate : Clock rate in kilohertz (default 100).
        Returns
        -------
            None
        """
        if size != self.kNi845xI2cAddress7Bit and size != self.kNi845xI2cAddress10Bit:
            raise Exception("Invalid address size")

        size = ctypes.c_int32(size)
        address = ctypes.c_uint16(address)
        clock_rate = ctypes.c_uint16(clock_rate)
        timeout = ctypes.c_uint32(timeout)
        #
        # create configuration reference
        #
        self.i2c_handle = ctypes.c_ulonglong()
        errChk(ni845x_dll.ni845xI2cConfigurationOpen(ctypes.byref(self.i2c_handle)))
        
        #
        # configure configuration properties
        #
        errChk(ni845x_dll.ni845xI2cConfigurationSetAddressSize(self.i2c_handle, size))
        errChk(ni845x_dll.ni845xI2cConfigurationSetAddress(self.i2c_handle, address))
        errChk(ni845x_dll.ni845xI2cConfigurationSetClockRate(self.i2c_handle, clock_rate))
        errChk(ni845x_dll.ni845xSetTimeout(self.dev_handle, timeout))

    def __open(self):
        self.dev_handle = ctypes.c_ulonglong()
        errChk(ni845x_dll.ni845xOpen(ctypes.byref(self.__name), ctypes.byref(self.dev_handle)))
    
    def dio_set_port_line_direction_map(self, mapp, port=0):
        # mapp: np array or list with 8 0's or 1's
        # 0 = input, 1 = output
        port = ctypes.c_uint8(port)
        mapp = np.asarray(mapp)
        assert len(mapp) == 8
        r = np.arange(7,-1,-1)
        _map = np.sum(2 ** r * mapp).astype(int)
        bitmap = ctypes.c_uint8(_map)
        errChk(ni845x_dll.ni845xDioSetPortLineDirectionMap(self.dev_handle, port, bitmap))

    def dio_write(self, mapp, port=0):
        port = ctypes.c_uint8(port)
        mapp = np.asarray(mapp)
        assert len(mapp) == 8
        r = np.arange(7,-1,-1)
        _map = np.sum(2 ** r * mapp).astype(int)
        bitmap = ctypes.c_uint8(_map)
        errChk(ni845x_dll.ni845xDioWritePort(self.dev_handle, port, bitmap))
        
    def dio_set_push_pull(self, port= 0):
        type = ctypes.c_uint8(0)
        port = ctypes.c_uint8(port)
        errChk(ni845x_dll.ni845xDioSetDriverType(self.dev_handle, port, type))

    def dio_set_voltage_level(self, lev):
        lev = ctypes.c_uint8(lev)
        errChk(ni845x_dll.ni845xSetIoVoltageLevel(self.dev_handle, lev))
    
    def close(self):
        errChk(ni845x_dll.ni845xClose(self.dev_handle))
        errChk(ni845x_dll.ni845xI2cConfigurationClose(self.i2c_handle))

    def dio_set_direction(self, port, dir):
        port = ctypes.c_uint8(port)
        dir = ctypes.c_uint8(dir)
        errChk(ni845x_dll.ni845xDioWriteLine(self.dev_handle, port, dir))

    def i2c_write(self, data):
        """
        Write an array of data to an I2C slave device.
        Parameters
        ----------
            write_data : Array of bytes to be written. Should be convertible to numpy array of
                    type unsigned char.
        Returns
        -------
            None
            
        """
        
        data = ctypes.create_string_buffer(data)
        nbytes = ctypes.c_uint32(len(data))

        errChk(ni845x_dll.ni845xI2cWrite(self.dev_handle, self.i2c_handle, nbytes, ctypes.byref(data)))
        
    def i2c_read(self, nbytes):
        """
        Read an array of data from an I2C slave device. Can read up to 16 bytes.
        Parameters
        ----------
            nbytes: Number of bytes to be read
        Returns
        -------
            List of bytes read
            
        """
        nbytes = ctypes.c_uint32(nbytes)
        data = ctypes.create_string_buffer(16)
        bytes_read = ctypes.c_uint32()

        errChk(ni845x_dll.ni845xI2cWrite(self.dev_handle, self.i2c_handle, nbytes, ctypes.byref(bytes_read), ctypes.byref(data)))
        
        return list(data.raw)[0:bytes_read - 1]

    def spi_config(self, word_lenght=8, clock_rate=1000, clock_polarity=0, clock_phase=0, timeout=2000):
        """
        Set the ni845x SPI configuration.
        
        Parameters
        ----------
            word_lenght : Size in bits of an SPI transmission. Default= 16
            clock_rate : Clock rate in kilohertz (default 1000).

        Returns
        -------
            None
        """
        word_lenght = ctypes.c_uint16(word_lenght)

        clock_polarity = ctypes.c_int32(clock_polarity)
        clock_phase = ctypes.c_int32(clock_phase)
        clock_rate = ctypes.c_int32(clock_rate)

        timeout = ctypes.c_uint32(timeout)
        #
        # create configuration reference
        #
        self.spi_handle = ctypes.c_ulonglong()
        errChk(ni845x_dll.ni845xSpiConfigurationOpen(ctypes.byref(self.spi_handle)))
        
        #
        # configure configuration properties
        #
        errChk(ni845x_dll.ni845xSpiConfigurationSetClockPolarity(self.spi_handle, clock_polarity))
        errChk(ni845x_dll.ni845xSpiConfigurationSetClockPhase(self.spi_handle, clock_phase))

        errChk(ni845x_dll.ni845xSpiConfigurationSetNumBitsPerSample(self.spi_handle, word_lenght))
        errChk(ni845x_dll.ni845xSetTimeout(self.dev_handle, timeout))

    def spi_writeread(self, data):
        if not isinstance(data, list):
            data = [data]
        nbytes = ctypes.c_uint32(len(data))
        data = ctypes.create_string_buffer(bytes(data), len(data))

        read_data = ctypes.create_string_buffer(128)
        read_data_size = ctypes.c_uint8(0)
        errChk(ni845x_dll.ni845xSpiWriteRead(self.dev_handle, self.spi_handle, nbytes, ctypes.byref(data), ctypes.byref(read_data_size), ctypes.byref(read_data)))

        return read_data.raw[0:read_data_size.value]

    def spi_set_CS(self, cs):
        chipselect = ctypes.c_uint32(cs)
        errChk(ni845x_dll.ni845xSpiConfigurationSetChipSelect (self.spi_handle, chipselect))

if __name__ == "__main__":
    nidev = NI845x()
    nidev.spi_config()
    #Set reset lines to 0 (reset is active high)
    nidev.dio_write([0,0,0,0,0,0,0,0])
    nidev.spi_set_CS(1)

    nidev.spi_writeread([0x40, 0x03])
    register_map = nidev.spi_writeread([0x00]*64 + [0x33])
    print(register_map.hex())
